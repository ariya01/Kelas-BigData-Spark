import os
import findspark
findspark.init()
from pyspark.sql.types import *
import logging
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.clustering import KMeans
from pyspark.ml.evaluation import ClusteringEvaluator
from math import sqrt

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class Cluster:

    def get_cluster_1(self,x,y,lat,ling):
        result = list()
        center = self.centers1
        for i in center:
            data = ((pow((float(i[0]) - float(lat)), 2)) + (pow((float(i[1]) - float(ling)), 2)) + (
                pow((float(i[2]) - float(x)), 2)) + (pow((float(i[3]) - float(y)), 2)))
            data = sqrt(data)
            result.append(data)
            print(result)

        result.sort()
        return result[0]

    def get_cluster_2(self, x, y, lat, ling):
        result = list()
        center = self.centers2
        for i in center:
            data = ((pow((float(i[0]) - float(lat)), 2)) + (pow((float(i[1]) - float(ling)), 2)) + (
                pow((float(i[2]) - float(x)), 2)) + (pow((float(i[3]) - float(y)), 2)))
            data = sqrt(data)
            result.append(data)
            print(result)

        result.sort()
        return result[0]

    def get_cluster_3(self, x, y, lat, ling):
        result = list()
        center = self.centers3
        for i in center:
            data = ((pow((float(i[0]) - float(lat)), 2)) + (pow((float(i[1]) - float(ling)), 2)) + (
                pow((float(i[2]) - float(x)), 2)) + (pow((float(i[3]) - float(y)), 2)))
            data = sqrt(data)
            result.append(data)
            print(result)

        result.sort()
        return result[0]

    def __train_model(self):
        logger.info("Train Data ...")
        logger.info("Make Model 1/3 ...")
        self.data1 = self.data.limit(int(self.dataset_count / 3))
        kmeans1 = KMeans().setK(10).setSeed(1)
        model1 = kmeans1.fit(self.data1)
        self.predict1 = model1.transform(self.data1)
        evaluator = ClusteringEvaluator()
        self.silhouette1 = evaluator.evaluate(self.predict1)
        logger.info("Silhouette with squared euclidean distance = " + str(self.silhouette1))
        self.centers1 = model1.clusterCenters()
        logger.info("Model 1/3 Done")

        logger.info("Make Model 2/3 ...")
        self.data2 = self.data.limit(int(self.dataset_count *2 / 3))
        kmeans2 = KMeans().setK(10).setSeed(1)
        model2 = kmeans2.fit(self.data2)
        self.predict2 = model2.transform(self.data2)
        evaluator = ClusteringEvaluator()
        self.silhouette2 = evaluator.evaluate(self.predict2)
        logger.info("Silhouette with squared euclidean distance = " + str(self.silhouette2))
        self.centers2 = model2.clusterCenters()
        logger.info("Model 2/3 Done")

        logger.info("Make Model 3/3 ...")
        self.data3 = self.data.limit(int(self.dataset_count ))
        kmeans3 = KMeans().setK(10).setSeed(1)
        model3 = kmeans3.fit(self.data3)
        self.predict3 = model3.transform(self.data3)
        evaluator = ClusteringEvaluator()
        self.silhouette3 = evaluator.evaluate(self.predict3)
        logger.info("Silhouette with squared euclidean distance = " + str(self.silhouette3))
        self.centers3 = model3.clusterCenters()
        logger.info("Model 3/3 Done")

        logger.info("Make Model Done")


    def __init__(self, sc, dataset_path):
        logger.info("Starting up the Recommendation Engine: ")
        self.sc = sc
        # Load ratings data for later use
        logger.info("Loading Ratings data...")
        file_counter = 0
        while True:
            file_name = 'data_part_' + str(file_counter) + '.txt'
            dataset_file_path = os.path.join(dataset_path, file_name)
            exist = os.path.isfile(dataset_file_path)
            if exist:
                if file_counter == 0:
                    self.data = sc.read.csv(dataset_file_path, header=None, inferSchema=True)
                else:
                    new_df = sc.read.csv(dataset_file_path, header=None, inferSchema=True)
                    self.data = self.data.union(new_df)
                self.dataset_count = self.data.count()
                print('dataset loaded = ' + str(self.dataset_count))
                print(file_name + 'Loaded !')
                file_counter += 1
            else:
                break
            logger.info("Done Load Data")

        logger.info("Give Name ...")
        self.data = self.data.selectExpr("_c0 as MASTER_INCIDENT_NUMBER", "_c1 as PRIORITY_DESCRIPTION",
                                         "_c2 as PROBLEM", "_c3 as ADDRESS", "_c4 as CALL_CLASS",
                                         "_c5 as TIME_PHONEPICKUP", "_c6 as CALL_DISPOSITION", "_c7 as GEO_X",
                                         "_c8 as GEO_Y", "_c9 as GEO_LON", "_c10 as GEO_LAT", "_c11 as DISTRICT_ID",
                                         "_c12 as PRECINCT_ID", "_c12 as NEIGHBORHOOD_NAME")
        logger.info("Done Give Name")
        self.data.createOrReplaceTempView("polisi")
        self.data = self.data.withColumn("lat", self.data["GEO_LON"].cast("double"))
        self.data = self.data.withColumn("Long", self.data["GEO_LAT"].cast("double"))
        assembler = VectorAssembler(inputCols=["GEO_LAT", "GEO_LAT", "GEO_X", "GEO_Y"], outputCol='features')
        self.data = assembler.transform(self.data)
        self.__train_model()
