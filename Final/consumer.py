from kafka import KafkaConsumer
import json
import os

consumer = KafkaConsumer(
    'final1',
    bootstrap_servers=['localhost:9092'],
    auto_offset_reset='latest',
    enable_auto_commit=True,
    value_deserializer=lambda m: json.loads(m.decode('utf-8')))

folder_path = os.path.join(os.path.dirname(os.getcwd()), 'Kafka')
batch_limit = 500000
batch_counter = 0
batch_number = 0

try:
    while True:
        for message in consumer:
            if batch_counter >= batch_limit:
                batch_counter = 0
                batch_number += 1
                writefile.close()
            if batch_counter == 0:
                file_path = os.path.join(folder_path, ('data_part_' + str(batch_number) + '.txt'))
                writefile = open(file_path, "w", encoding="utf-8")
            message = message.value
            writefile.write(message)
            batch_counter += 1
            print('current batch : ' + str(batch_number) + ' current data for this batch : ' + str(batch_counter))
except KeyboardInterrupt:
    writefile.close()
print('Keyboard Interrupt called by user, exiting.....')