# Final Project Big Data
## Data Set

### Judul Data set
Denver Police Pedestrian Stops and Vehicle Stops

### Deskripsi Data set
Data set yang berisi tentang pemberhentian atau penilangan kendaraan dan orang oleh Polisi Denver

### Link
https://www.kaggle.com/paultimothymooney/police-pedestrian-stops-and-vehicle-stops

## File 
`Produser.py`

Sistem pada Kafka yang akan membaca dan  mengirimkan data ke Consumer
 
`Consumer.py`

Sistem pada Kafka yang akan akan menerima file dari produser dan menyimpannya

`Server.py`

Digunakan untuk membuat sever dengan memaanggil App.py

`App.py`

Akan menjadi route untuk server

`Enngine.py`

Digunakan untuk menjadi sistem pemrosesan Back end pada server

## Model yang digunakan 

1. Model 1: 1/3 data 
2. Model 2: 2/3 data
3. Model 3: semua data


## Menjalankan Sistem

1. Menyalakan Zookepper
    1. Jalankan langsung `zkserver`
    
2. Menjalankan Kafka
    1. Masuk ke directori `kafka`
    2. Jalankan ` .\bin\windows\kafka-server-start.bat .\config\server.properties`
    
3. Membuat Topik
    1. Masuk ke directory `kafka\bin\windows`
    2. Jalankan `kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic final`
    
4. Jalankan Produser
    * Untuk mencoba dengan Cmd
        1. Masuk ke directory `kafka\bin\windows`
        2. Jalankan ` kafka-console-producer.bat --broker-list localhost:9092 --topic final`
    * Menjalakan dengan File
        1. Jalankan `python Produser.py` 
    
5.  Jalakan Consumer
    * Untuk mencoba dengan Cmd
        1. Masuk ke directory `kafka\bin\windows`
        2. Jalankan `kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic final --from-beginning`
    * Menjalankan dengan File
        1. Jalankan `python consumer.py`
    
6.  Jalankan Server
    1. Jalankan `python server.py`
    
 
 ## Hasil 
 ![alt text][logo]

[logo]: https://raw.githubusercontent.com/ariya01/Kelas-BigData-Spark/master/Gambar/Final.PNG "Logo Title Text 2"
