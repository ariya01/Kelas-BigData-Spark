from flask import Blueprint

main = Blueprint('main', __name__)

import json
from engine import Cluster

import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

from flask import Flask, request


@main.route("/", methods=["GET"])
def get_ratings():
    logger.debug("Check API")
    data = {"hasil":"selamat datang"}
    return json.dumps(top_rated)

@main.route("/cluster/model1", methods=["POST"])
def get_model1():
    logger.debug("Get Cluster")
    address = request.form.get('address')
    x = float(request.form.get('geo_x'))
    y = float(request.form.get('geo_y'))
    lat = float(request.form.get('geo_lat'))
    long = float(request.form.get('geo_long'))
    if (x!=None,y!=None,lat!=None,long!=None):
        res = recommendation_engine.get_cluster_1(x,y,lat,long)
        data = {"hasil":res,"address":address}
    else:
        data = {"hasil": "Parameter Tidak Lengkap"}
    return json.dumps(data)

@main.route("/cluster/model2", methods=["POST"])
def get_model2():
    logger.debug("Get Cluster")
    address = request.form.get('address')
    x = float(request.form.get('geo_x'))
    y = float(request.form.get('geo_y'))
    lat = float(request.form.get('geo_lat'))
    long = float(request.form.get('geo_long'))
    if (x!=None,y!=None,lat!=None,long!=None):
        res = recommendation_engine.get_cluster_2(x,y,lat,long)
        data = {"hasil":res,"address":address}
    else:
        data = {"hasil": "Parameter Tidak Lengkap"}
    return json.dumps(data)

@main.route("/cluster/model3", methods=["POST"])
def get_model3():
    logger.debug("Get Cluster")
    address = request.form.get('address')
    x = float(request.form.get('geo_x'))
    y = float(request.form.get('geo_y'))
    lat = float(request.form.get('geo_lat'))
    long = float(request.form.get('geo_long'))
    if (x!=None,y!=None,lat!=None,long!=None):
        res = recommendation_engine.get_cluster_3(x,y,lat,long)
        data = {"hasil":res,"address":address}
    else:
        data = {"hasil": "Parameter Tidak Lengkap"}
    return json.dumps(data)

def create_app(spark_session, dataset_path):
    global recommendation_engine

    recommendation_engine = Cluster(spark_session, dataset_path)

    app = Flask(__name__)
    app.register_blueprint(main)
    return app
