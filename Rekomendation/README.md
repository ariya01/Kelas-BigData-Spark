# API Rekomendasi

Karena tidak adanya keterangan produk yang ditampilkan hanya Product_Id saja

Base URL = http://127.0.0.1:8000/

1. URL = `BaseURL`+/:id_user/ratings/top/:jumlah_data
    
    Method = `GET` 
    
    Keterangan = Digunakan untuk mendapatan rating tertinggi

    ![alt text](https://raw.githubusercontent.com/ariya01/Kelas-BigData-Spark/master/Gambar/toprating.PNG)

2. URL = `BaseURL`+/:id_user/recomend/top/:jumlah_data
    
    Method = `GET` 
    
    Keterangan = Digunakan untuk mendapatan rekomendasi product
    ![alt text](https://raw.githubusercontent.com/ariya01/Kelas-BigData-Spark/master/Gambar/toprecomend.PNG)

2. URL = `BaseURL`+/rating/:Product_id
    
    Method = `GET` 
    
    Keterangan = Digunakan untuk mengetahui nulai kedekatan kita dengan product
    ![alt text](https://raw.githubusercontent.com/ariya01/Kelas-BigData-Spark/master/Gambar/rating.PNG)