import os
import findspark
findspark.init()
from pyspark.mllib.recommendation import ALS
from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.recommendation import ALS
from pyspark.sql import Row
import pandas as pd
from pyspark.sql.types import *
import pandas as pd
import logging
from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.recommendation import ALS
from pyspark.sql import Row
from pyspark.sql import types
from pyspark.sql.functions import explode
import pyspark.sql.functions as func

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def get_counts_and_averages(ID_and_ratings_tuple):
    """Given a tuple (movieID, ratings_iterable) 
    returns (movieID, (ratings_count, ratings_avg))
    """
    nratings = len(ID_and_ratings_tuple[1])
    return ID_and_ratings_tuple[0], (nratings, float(sum(x for x in ID_and_ratings_tuple[1]))/nratings)


class RecommendationEngine:
    def get_top_movie_recommend(self, product_id, user_count):
        product = self.olah2.select(self.als.getItemCol())
        product = product.filter(product.ProductId == product_id)
        productubsetRecs = self.model.recommendForItemSubset(product, user_count)
        productubsetRecs = productubsetRecs.withColumn("recommendations", explode("recommendations"))
        productubsetRecs = productubsetRecs.select(func.col('ProductId'),
                                             func.col('recommendations')['userId'].alias('userId'),
                                             func.col('recommendations')['Rating'].alias('Rating'))
        productubsetRecs = productubsetRecs.drop('Rating')
        productubsetRecs = productubsetRecs.drop('userId')
        productubsetRecs = productubsetRecs.toPandas()
        productubsetRecs = productubsetRecs.to_json()
        return productubsetRecs

    def get_rating_for_user(self, movie_id):
        product = self.olah2.select(self.als.getItemCol())
        product = product.filter(product.ProductId == movie_id)
        productubsetRecs = self.model.recommendForItemSubset(product, 1)
        productubsetRecs = productubsetRecs.select(func.col('ProductId'),
                                                   func.col('recommendations')['userId'].alias('userId'),
                                                   func.col('recommendations')['Rating'].alias('Rating'))
        productubsetRecs = productubsetRecs.drop('ProductId')
        productubsetRecs = productubsetRecs.drop('userId')
        productubsetRecs = productubsetRecs.toPandas()
        productubsetRecs = productubsetRecs.to_json()
        return productubsetRecs

    def get_top_ratings(self,user_id,procut_count):
        users = self.olah2.select(self.als.getUserCol())
        users = users.filter(users.UserId == user_id)
        userSubsetRecs = self.model.recommendForUserSubset(users, procut_count)
        userSubsetRecs = userSubsetRecs.withColumn("recommendations", explode("recommendations"))
        userSubsetRecs = userSubsetRecs.select(func.col('userId'),
                                               func.col('recommendations')['ProductId'].alias('ProductId'),
                                               func.col('recommendations')['Rating'].alias('Rating'))
        userSubsetRecs = userSubsetRecs.drop('Rating')
        userSubsetRecs = userSubsetRecs.drop('userId')
        userSubsetRecs = userSubsetRecs.toPandas()
        userSubsetRecs = userSubsetRecs.to_json()
        return userSubsetRecs
    def __train_model(self):
        """Train the ALS model with the current dataset
        """
        logger.info("Training the ALS model...")
        self.als = ALS(maxIter=5, regParam=0.01, userCol="UserId", itemCol="ProductId", ratingCol="Score",coldStartStrategy="drop")
        self.model = self.als.fit(self.olah2)
        logger.info("ALS model built!")

    def __init__(self, sc, dataset_path):
        """Init the recommendation engine given a Spark context and a dataset path
        """

        logger.info("Starting up the Recommendation Engine: ")

        self.sc = sc

        # Load ratings data for later use
        logger.info("Loading Ratings data...")
        ratings_file_path = os.path.join(dataset_path, 'Reviews.csv')
        data = pd.read_csv(ratings_file_path)
        logger.info("Loading Data Done")
        olah = data.drop(['ProfileName', 'HelpfulnessNumerator', 'HelpfulnessDenominator', 'Summary', 'Text', 'Id', 'Time'], axis=1)
        olah.reset_index()
        olah['Score'] = olah['Score'].astype(float)
        olah['ProductId'] = olah['ProductId'].astype('category').cat.codes
        olah['UserId'] = olah['UserId'].astype('category').cat.codes
        olah['ProductId'] = olah['ProductId'].astype(int)
        olah['UserId'] = olah['UserId'].astype(int)
        mySchema = StructType(
            [StructField("ProductId", IntegerType(), True), StructField("UserId", IntegerType(), True),
             StructField("Score", FloatType(), True)])
        self.olah2 = sc.createDataFrame(olah, schema=mySchema)
        self.__train_model()
